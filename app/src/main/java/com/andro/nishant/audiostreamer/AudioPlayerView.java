package com.andro.nishant.audiostreamer;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;
import android.media.MediaPlayer.OnBufferingUpdateListener;

/**
 * Created by NishantThite on 01/07/15.
 */
public class AudioPlayerView extends View implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
        SeekBar.OnSeekBarChangeListener, OnBufferingUpdateListener{

    public int bufferPercent = 0;
    MainActivity mainActivityObj;
    MediaPlayer mp;
    SeekBar audioSeekBar;
    final String SongUrl = "http://gostories.co.in/Sandeep/prastawana.mp3";
    final String LogTag = "StreamAudioDemo";
    Context context;
    public Button playOrPauseBtn;


    public AudioPlayerView(Context context) {
        super(context);
        this.context = context;
    }

    public void stopAudioPlayer(){
        if(mp != null){
            mp.stop();
            mp.reset();
        }
        if (audioSeekBar != null){
            audioSeekBar.setProgress(0);
        }
    }

    public void playOrPauseAudioPlayer(){
        if (playOrPauseBtn != null){
            if (playOrPauseBtn.getText().toString().equalsIgnoreCase("Play")){
                if(mp != null){
                    mp.start();
                }

                playOrPauseBtn.setText("Pause");
            }else if (playOrPauseBtn.getText().toString().equalsIgnoreCase("Pause")){
                if(mp != null){
                    mp.pause();
                }

                playOrPauseBtn.setText("Play");
            }else {
                if(mp != null){
                    mp.reset();
                }

                playOrPauseBtn.setText("Play");
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {

        audioSeekBar.setOnSeekBarChangeListener(this);
        audioSeekBar.setProgress(0);
        audioSeekBar.setSecondaryProgress(bufferPercent);
        audioSeekBar.setMax(mp.getDuration());
        audioSeekBar.postDelayed(onEverySecond, 1000);

        Toast.makeText(context, "Prepare finished", Toast.LENGTH_LONG).show();
        Log.i(LogTag, "Prepare finished");
        mainActivityObj.pd.setMessage("Playing.....");
        mainActivityObj.pd.dismiss();
        mp.start();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {

        String errorStr = "onError MediaPlayer : "+ what+" and "  + extra;
        mainActivityObj.errorTextView.setText(errorStr);
        Toast.makeText(context, errorStr, Toast.LENGTH_LONG).show();
        mainActivityObj.pd.dismiss();

        mp.release();
        audioSeekBar.setProgress(0);
        bufferPercent = 0 ;
        audioSeekBar.setSecondaryProgress( bufferPercent);
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mainActivityObj.pd.dismiss();
        //Toast.makeText(getApplicationContext(), "Completed", Toast.LENGTH_LONG).show();
    }

    private Runnable onEverySecond=new Runnable() {
        @Override
        public void run() {

            if(audioSeekBar != null && mp.isPlaying()) {
                audioSeekBar.setProgress(mp.getCurrentPosition());
                audioSeekBar.setSecondaryProgress( bufferPercent);

                audioSeekBar.postDelayed(onEverySecond, 1000);
            }
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            // this is when actually seekbar has been seeked to a new position
            mp.seekTo(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

        bufferPercent = percent;
        mainActivityObj.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                audioSeekBar.setIndeterminate(false);
                audioSeekBar.setSecondaryProgress(bufferPercent);
                String updateMsg = "\n buffer Percent: " + bufferPercent+ "\n\n";
                mainActivityObj.errorTextView.setText(updateMsg);
                Log.d(LogTag, updateMsg );
            }
        });
    }
}
