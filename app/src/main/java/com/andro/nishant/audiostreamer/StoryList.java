package com.andro.nishant.audiostreamer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by NishantThite on 08/07/15.
 */
public class StoryList extends Activity {

    ListView storyListView;
    ProgressDialog pdInStoryList;
    String strJson;
    ArrayList<String> storyListValues=new ArrayList<String>();
    ArrayAdapter<String> storyArrayAdapter;
    int newBackGrounfColorForStoryList = 0;
    final String Appery_Database_Request_Header = "X-Appery-Database-Id";
    final String Appery_Database_Id = "552f2a21e4b04b730cfa1342";

    final String story_name = "story_name";
    final String book_name = "book_name";
    final String story_duration = "story_duration";
    final String director = "director";
    final String author_point = "author_point";
    final String author_name = "author_name";

    String appVersionNameStr;

    final String plainStroryListApiBaseUrlStr = "https://api.appery.io/rest/1/db/collections/stories/";
    final String pointerStroryListApiBaseUrlStr = "https://api.appery.io/rest/1/db/collections/stories?include=language_point%2Cauthor_point%2Cvoice_artist";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_list);

        String stringData = "";
        Intent intent = getIntent();
        if (null != intent) {
            stringData = intent.getStringExtra("USERNAME");

        }

        Log.d(MainActivity.LogTag,"stringData");

        storyListView = (ListView) findViewById(R.id.storyListViewId);

        // Defined Array values to show in ListView
        storyListValues.add("Test - 1");
        storyListValues.add("Test - 2");
        storyListValues.add("Test - 3");
        storyListValues.add("Test - 4");
        storyListValues.add("Test - 5");
        storyListValues.add("Test - 6");
        storyListValues.add("Test - 7");
        storyListValues.add("Test - 9");
        storyListValues.add("Test - 10");
        storyListValues.add("Test - 11");
        storyListValues.add("Test - 12");
        storyListValues.add("Test - 13");
        storyListValues.add("Test - 14");
        storyListValues.add("Test - 15");


        storyArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, storyListValues);

        // Assign adapter to ListView
        storyListView.setAdapter(storyArrayAdapter);

        //int versionCode = BuildConfig.VERSION_CODE;
        appVersionNameStr = BuildConfig.VERSION_NAME;

        // ListView Item Click Listener
        storyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) storyListView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "App version : " + appVersionNameStr + "\nClicked on : " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });
    }

    public void loadStoryDataOnBackThread (View v){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                loadJsonString();
            }
        });
        thread.start();
    }

    public void loadStoryAsAsyncTask(View v){

        String[] newValueArr = {"Loading.."};
        updateListViewWithDataAndBackGroundColor(newValueArr, Color.GREEN);

        pdInStoryList = new ProgressDialog(this);
        pdInStoryList.setMessage("Loading story list..");
        pdInStoryList.setCancelable(false);
        pdInStoryList.show();
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                //loadStoryDataOnBackThread(v);
                new DownloadWebpageTask().execute();
            } else {
                // display error
                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                }

                Log.e(MainActivity.LogTag, errMsg);

               // String[] errArr = {errMsg};
               // updateListViewWithDataAndBackGroundColor(errArr, Color.RED);
            }
        } catch (Exception e) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + e+"\n\n";
            // this.errorTextView.setText(errStr);
            Log.e(MainActivity.LogTag, errStr);
        }

    }

    public void updateListViewWithDataAndBackGroundColor(String[] detailsArr, int newBgColor){
        storyListValues.clear();

        for (int j=0; j<detailsArr.length ; j++){
            storyListValues.add(detailsArr[j]);
        }

        newBackGrounfColorForStoryList = newBgColor;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run( ) {
                if (pdInStoryList != null){
                    pdInStoryList.dismiss();
                }
                storyListView.setBackgroundColor(newBackGrounfColorForStoryList);
                storyArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private class DownloadWebpageTask extends AsyncTask<String, String, String> {
        String  serverResponse;

        @Override
        protected String doInBackground(String... urls) {

           // Log.e(MainActivity.LogTag, "\n\n"+"doInBackground"+"\n\n");
            String str= "";
            // params comes from the execute() call: params[0] is the url.
            try {

//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
                // Create a URL for the desired page

                URI uri = new URI(pointerStroryListApiBaseUrlStr);
                String serverAddress = uri.toString();
                URL url = new URL(serverAddress);

               // URL url = new URL(stroryListApiBaseUrl);

                // Open an HTTP  connection to  the URTL
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
               // conn.setDoOutput(true); // Only use for POST requests
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestProperty(Appery_Database_Request_Header, Appery_Database_Id);
               // conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestMethod("GET");
                conn.connect();

                int lengthOfFile = conn.getContentLength();
                //Log.e(MainActivity.LogTag, "\n lengthOfFile : " + lengthOfFile + " \n");

                int serverResponseCode = conn.getResponseCode();
                String servResp = conn.getResponseMessage();
                serverResponse = "serverResponse: "+serverResponseCode+", "+servResp;
               // Log.d(MainActivity.LogTag, "serverResponse : " + serverResponse);

                //InputStream input = new BufferedInputStream(url.openStream(),10*1024);
                BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()),10*1024);
                //InputStream input = new BufferedInputStream(url.openStream(),10*1024);
                // Output stream to write file in SD card

                byte data[] = new byte[1024];
                long currentProgress = 0;
                int count = 0;
                strJson="";

//                long total = 0;
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // Publish the progress which triggers onProgressUpdate method
//                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
//
//                    // Write data to file
//                    output.write(data, 0, count);
//                }

                while ((str = input.readLine()) != null) {
                    strJson = str;
                    currentProgress += count;
                    // Publish the progress which triggers onProgressUpdate method
                    publishProgress("" + ((currentProgress * 100) / lengthOfFile));
                    // Write data to file
                }

               // Log.d(MainActivity.LogTag, "\n\n strJson : " + strJson+"\n\n");


                input.close();
                conn.disconnect();
            } catch (Exception e) {
                String errStr = "Error in load Story JsonString: " + e;
                Log.e(MainActivity.LogTag, errStr);
            }
            return strJson;
        }

        protected void onProgressUpdate (String progressValue){
            Log.e(MainActivity.LogTag, "\n progressValue: "+progressValue+"\n");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (pdInStoryList != null){
                pdInStoryList.dismiss();
            }

            try{
               String fileName = "stroyListJson.txt";
                File reqFile = createFileUnderGoStoriesDir( fileName);

                if (reqFile != null){
                    FileWriter writer = new FileWriter(reqFile.getAbsolutePath());
                    writer.append(strJson);
                    writer.flush();
                    writer.close();
                }else {
                    Log.e(MainActivity.LogTag, "Could not save file on sd");
                }
               // JSONObject tempObj = new JSONObject(strJson);
                // String tmpStr = tempObj.toString(3);
               // Log.e(MainActivity.LogTag, strJson);

            }catch (Exception e){

                String errStr = "\n\nError in Saving JsonString to file: " + e +"\n\n";
                Log.e(MainActivity.LogTag, errStr);

            }

           // String[] newValueArr = {serverResponse};
           // updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);
            parseMyStoryListJson();
        }
    }

    private String downloadUrl(String myUrl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(MainActivity.LogTag, "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    public void loadJsonString(){
        try {

//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);

            // Create a URL for the desired page
            URL url = new URL("https://iosish.iriscouch.com/nishant/TestNishant");

            Log.e(MainActivity.LogTag, "\nLoding Story data from server\n");

            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;
            while ((str = in.readLine()) != null) {
                // str is one line of text; readLine() strips the newline character(s)
                // Log.e(LogTag, "\nData from server : "+str+"\n");
                strJson = str;
            }
            Log.e(MainActivity.LogTag, "\nStory data: " + strJson + "\n");
            // strJson = str;
            in.close();


            if(strJson.length()>1){
                parseMyStoryListJson();
            }
        } catch (Exception e) {
            String errStr = "Error in load Story JsonString: " + e;
            // this.errorTextView.setText(errStr);
            Log.e(MainActivity.LogTag, errStr);
        }
    }

    public File createFileUnderGoStoriesDir(String fileName)
    {
        File storyListLocalJSONFile = null;
        try{
            File GoStoriesDir = new File(Environment.getExternalStorageDirectory().getPath()+File.separator+"GoStories");
            if (!GoStoriesDir.exists()) {
                if(GoStoriesDir.mkdir()){
                    Log.e(MainActivity.LogTag, "\n GoStories directory creation SUCCESS \n");
                }else {
                    Log.e(MainActivity.LogTag, "\n GoStories directory creation FAILED \n");
                }
            }
            storyListLocalJSONFile = new File(GoStoriesDir+File.separator+fileName);

            if(storyListLocalJSONFile.exists()){
                Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile created \n");
            }else {
                if (storyListLocalJSONFile.createNewFile()) {
                    Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile creation SUCCESS \n");
                }else {
                    Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile creation FAILED \n");
                }
            }
        }catch (Exception e){

        }
        return storyListLocalJSONFile;
    }

    public void parseMyStoryListJson() {
        try {
            JSONArray rootJsonArray = new JSONArray(strJson);

            //Get the instance of JSONArray that contains JSONObjects
          //  JSONArray jsonArray = jsonRootObject.optJSONArray("Employee");
          //  jsonRootObject.getJSONArray();
            String[] newValueArr = new String[rootJsonArray.length()];
            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < rootJsonArray.length(); i++){
                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                String storyNameStr = jsonObject.optString(story_name);
                JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                String authorNameStr = author_pointJsonObject.optString(author_name);
                newValueArr[i] = storyNameStr+ " : "+ authorNameStr;
            }

            updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);

        } catch (JSONException e) {
            String errStr = "Error in parseMyJson in StoryList: " + e.getMessage();
            //this.errorTextView.setText(errStr);
            Log.e(MainActivity.LogTag, errStr);
        }
    }
}
